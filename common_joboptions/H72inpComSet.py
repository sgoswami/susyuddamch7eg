import os
evt_multiplier = 5
def writeH72inFile(runArgs,run_name,infile_name="Herwig.in"):
    if os.access(infile_name, os.R_OK):
        os.unlink(infile_name)

    print("MESSAGE: Existing Herwig input file not found, can write a new file.")
    with open(infile_name,"w") as f:
        f.write(
            """
## =========================================
## Global Pre-Commands
## =========================================

## ensure JetFinder uses AntiKt with R=0.4
set /Herwig/Cuts/JetFinder:Variant AntiKt
set /Herwig/Cuts/JetFinder:ConeRadius 0.4


## Random number generator seed
set /Herwig/Random:Seed {rndseed}


## Verbosity and printout settings
set /Herwig/Generators/EventGenerator:DebugLevel 1
set /Herwig/Generators/EventGenerator:PrintEvent 1
set /Herwig/Generators/EventGenerator:UseStdout Yes
set /Herwig/Generators/EventGenerator:NumberOfEvents {evts}
set /Herwig/Generators/EventGenerator:MaxErrors 1000

## Make sampler print out cross sections for each subprocess
set /Herwig/Samplers/Sampler:Verbose Yes

## Set long-lived particles stable
set /Herwig/Decays/DecayHandler:MaxLifeTime 10*mm

## ========================
## Commands from jobOptions
## ========================


################################
## Read in Events from LHE File
################################

##################################################
#   Create the Les Houches file handler and reader
##################################################
cd /Herwig/EventHandlers
library LesHouches.so
# create the event handler
create ThePEG::LesHouchesEventHandler LesHouchesHandler

# set the various step handlers
set LesHouchesHandler:PartonExtractor /Herwig/Partons/PPExtractor
set LesHouchesHandler:CascadeHandler /Herwig/Shower/ShowerHandler
set LesHouchesHandler:DecayHandler /Herwig/Decays/DecayHandler
set LesHouchesHandler:HadronizationHandler /Herwig/Hadronization/ClusterHadHandler

# set the weight option
set LesHouchesHandler:WeightOption VarNegWeight

# set event hander as one to be used
set /Herwig/Generators/EventGenerator:EventHandler /Herwig/EventHandlers/LesHouchesHandler

# Set up an EMPTY CUTS object
# Normally you will have imposed any cuts you want
# when generating the event file and don't want any more
# in particular for POWHEG and MC@NLO you must not apply cuts on the
# the extra jet
create ThePEG::Cuts /Herwig/Cuts/NoCuts

###########################################################
## PDF Settings
##########################################################
## Configure NLO PDF set for the hard process
create ThePEG::LHAPDF /Herwig/Partons/LHAPDF ThePEGLHAPDF.so
set /Herwig/Partons/LHAPDF:PDFName {pdfName}
set /Herwig/Partons/RemnantDecayer:AllowTop Yes
set /Herwig/Partons/LHAPDF:RemnantHandler /Herwig/Partons/HadronRemnants
set /Herwig/Particles/p+:PDF /Herwig/Partons/LHAPDF
set /Herwig/Particles/pbar-:PDF /Herwig/Partons/LHAPDF
set /Herwig/Partons/PPExtractor:FirstPDF  /Herwig/Partons/LHAPDF
set /Herwig/Partons/PPExtractor:SecondPDF /Herwig/Partons/LHAPDF


# We would recommend the shower uses the default PDFs with which it was tuned.
# However it can be argued that the same set as for the sample should be used for
# matched samples, i.e. MC@NLO (and less so POWHEG)

set /Herwig/Shower/ShowerHandler:PDFA /Herwig/Partons/LHAPDF
set /Herwig/Shower/ShowerHandler:PDFB /Herwig/Partons/LHAPDF
############################################################
## LHEREADER Settings: LHEREADER DECLARED WITH LHE HANDLER
############################################################
# Create a LH reader

create ThePEG::LesHouchesFileReader LesHouchesReader
set LesHouchesReader:FileName {FileName}

set LesHouchesReader:AllowedToReOpen No
set LesHouchesReader:InitPDFs 0
set LesHouchesReader:Cuts /Herwig/Cuts/NoCuts
set LesHouchesReader:IncludeSpin {IncludeSpin}

# option to ensure momentum conservation is O.K. due rounding errors (recommended)
set LesHouchesReader:MomentumTreatment RescaleEnergy

# set the pdfs
set LesHouchesReader:PDFA /Herwig/Partons/LHAPDF
set LesHouchesReader:PDFB /Herwig/Partons/LHAPDF

# if using BSM models with QNUMBER info
set LesHouchesReader:QNumbers Yes
set LesHouchesReader:Decayer /Herwig/Decays/Mambo

#Add to Handler
insert LesHouchesHandler:LesHouchesReaders 0 LesHouchesReader

##################################################
#  Shower parameters
##################################################
# normally, especially for POWHEG, you want
# the scale supplied in the event files (SCALUP)
# to be used as a pT veto scale in the parton shower
set /Herwig/Shower/ShowerHandler:MaxPtIsMuF Yes
set /Herwig/Shower/ShowerHandler:RestrictPhasespace Yes

# Shower parameters
# treatment of wide angle radiation
set /Herwig/Shower/PartnerFinder:PartnerMethod Random
set /Herwig/Shower/PartnerFinder:ScaleChoice Partner

## Commands specific to showering of events produced with MG5_aMC@NLO
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
set /Herwig/Shower/KinematicsReconstructor:InitialStateReconOption Rapidity
set /Herwig/Shower/ShowerHandler:SpinCorrelations No
set /Herwig/Shower/ShowerHandler:MaxTry  100

##########################################################
# More Shower Stuff
##########################################################
set /Herwig/Shower/ShowerHandler:Interactions QCDandQED

####################
## HEP MC
####################
insert /Herwig/Generators/EventGenerator:AnalysisHandlers[0] /Herwig/Analysis/HepMCFile
set /Herwig/Analysis/HepMCFile:PrintEvent 100
set /Herwig/Analysis/HepMCFile:Format GenEvent
set /Herwig/Analysis/HepMCFile:Units GeV_mm

###############################################################
## Quick "fix" to the mismatch between Herwig 7 and EvtGen of the masses below
set /Herwig/Particles/B'_c1+:NominalMass 7.3
set /Herwig/Particles/B'_c1-:NominalMass 7.3
set /Herwig/Particles/B_c1+:NominalMass 7.3
set /Herwig/Particles/B_c1-:NominalMass 7.3
set /Herwig/Particles/B_c2+:NominalMass 7.35
set /Herwig/Particles/B_c2-:NominalMass 7.35
set /Herwig/Particles/B*_c0+:NominalMass 7.25
set /Herwig/Particles/B*_c0-:NominalMass 7.25
set /Herwig/Particles/B_c+:NominalMass 6.277
set /Herwig/Particles/B_c-:NominalMass 6.277

set /Herwig/Particles/D'_s1+:NominalMass 2.4595000e+00
set /Herwig/Particles/D'_s1+:Width 0.001
set /Herwig/Particles/D'_s1+:WidthCut 0.01
set /Herwig/Particles/D'_s1+:Width_generator:Initialize Yes
set /Herwig/Particles/D'_s1+:Mass_generator:Initialize Yes
set /Herwig/Particles/D'_s1-:NominalMass 2.4595000e+00
set /Herwig/Particles/D'_s1-:Width 0.001
set /Herwig/Particles/D'_s1-:WidthCut 0.01
set /Herwig/Particles/D'_s1-:Width_generator:Initialize Yes
set /Herwig/Particles/D'_s1-:Mass_generator:Initialize Yes
set /Herwig/Particles/D_s1+:NominalMass 2.5352800e+00
set /Herwig/Particles/D_s1+:Width 0.001
set /Herwig/Particles/D_s1+:WidthCut 0.01
set /Herwig/Particles/D_s1+:Width_generator:Initialize Yes
set /Herwig/Particles/D_s1+:Mass_generator:Initialize Yes
set /Herwig/Particles/D_s1-:NominalMass 2.5352800e+00
set /Herwig/Particles/D_s1+:Width 0.001
set /Herwig/Particles/D_s1+:WidthCut 0.01
set /Herwig/Particles/D_s1+:Width_generator:Initialize Yes
set /Herwig/Particles/D_s1+:Mass_generator:Initialize Yes
set /Herwig/Particles/D'_s1-:Stable Stable
set /Herwig/Particles/D'_s1+:Stable Stable
set /Herwig/Particles/D_s1-:Stable Stable
set /Herwig/Particles/D_s1+:Stable Stable


## =============================================
## Local RUN Commands
## =============================================
cd /Herwig/Generators
saverun {RUNNAME} /Herwig/Generators/EventGenerator
""".format(
                rndseed=runArgs.randomSeed,
                evts= runArgs.maxEvents * evt_multiplier,
                pdfName="NNPDF30_nlo_as_0118",
                FileName="tmp_LHE_events.events",
                IncludeSpin="No",
                RUNNAME=run_name
            )
        )
