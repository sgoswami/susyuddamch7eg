## \file Herwig7Control.py
## \brief Main python interface for %Herwig7 for preparing the event generation
## \author Daniel Rauch (daniel.rauch@desy.de)
## \author Soumyananda Goswami (soumyananda.goswami@cern.ch)
##
## This part of the interface provides functionality for running all the tasks
## necessary to initialize and prepare the event generation.
## C++ items are in:
## in Herwig7_i/Herwig7.h and src/Herwig7.cxx.

import datetime, os, shutil, subprocess, sys, time
import six
from H72inpComSet import writeH72inFile as inpcom
import H72Utils as hw7Utils

from AthenaCommon import Logging

athMsgLog = Logging.logging.getLogger("Herwig72UserDefinedControl")


## \brief Get path to the `share/Herwig` folder
##
## Try to get it from the `InstallArea` first.
## If this fails fall back to `$HERWIG7_PATH/share/Herwig`
##
def get_share_path():

    cmt_paths = os.environ.get("CMAKE_PREFIX_PATH")
    cmt_config = os.environ.get("BINARY_TAG")

    # trying to get it from the `InstallArea`
    for path in cmt_paths.split(":"):
        path = os.path.join(path, "InstallArea", cmt_config, "share")
        try:
            filelist = os.listdir(path)
        except:
            filelist = []
        if "HerwigDefaults.rpo" in filelist:
            return path

    # falling back to `$HERWIG7_PATH`
    path = os.path.join(os.environ["HERWIG7_PATH"], "share/Herwig")
    if os.path.isfile(os.path.join(path, "HerwigDefaults.rpo")):
        return path

    # raise exception if none of the two methods work out
    raise RuntimeError(
        hw7Utils.ansi_format_error("Could not find a valid share/Herwig folder")
    )


# proper handling with path set in External/Herwig7/cmt/requirements
herwig7_path = os.environ["HERWIG7_PATH"]
herwig7_bin_path = os.path.join(herwig7_path, "bin")
herwig7_share_path = get_share_path()

herwig7_binary = os.path.join(herwig7_bin_path, "Herwig")


## Do the read/run sequence.
##
## This function should provide the read and run step in one go
def run(gen_config):

    ## perform the read step
    do_read(gen_config)

    ## start the event generation
    do_run(gen_config, cleanup_herwig_scratch=False)


def do_step(step, command, logfile_name=None):

    athMsgLog.info(
        hw7Utils.ansi_format_info(
            "Starting Herwig7 '{}' step with command '{}'".format(
                step, " ".join(command)
            )
        )
    )

    logfile = open(logfile_name, "w") if logfile_name else None
    do = subprocess.Popen(command, stdout=logfile, stderr=logfile)
    do.wait()
    if not do.returncode == 0:
        raise RuntimeError(
            hw7Utils.ansi_format_error(
                "Some error occured during the '{}' step.".format(step)
            )
        )

    if logfile:
        athMsgLog.info("Content of {} log file '{}':".format(step, logfile_name))
        athMsgLog.info("")
        with open(logfile_name, "r") as logfile:
            for line in logfile:
                athMsgLog.info("  {}".format(line.rstrip("\n")))
        athMsgLog.info("")


def do_abort():
    athMsgLog.info(hw7Utils.ansi_format_info("Aborting run"))
    sys.exit(0)


## Do the read step
def do_read(gen_config):

    ## print start banner including version numbers
    log(message=start_banner())

    ## create infile from JobOption object
    write_infile(gen_config)

    ## copy HerwigDefaults.rpo to the current working directory
    get_default_repository()

    ## call Herwig7 binary to do the read step
    share_path = get_share_path()
    do_step(
        "read",
        [
            herwig7_binary,
            "read",
            get_infile_name(gen_config.run_name),
            "-I",
            share_path,
        ],
    )


## Do the read step and re-use an already existing infile
def do_read_existing_infile(gen_config):

    ## print start banner including version numbers
    log(message=start_banner())

    ## copy HerwigDefaults.rpo to the current working directory
    get_default_repository()

    ## call Herwig7 binary to do the read step
    share_path = get_share_path()
    do_step("read", [herwig7_binary, "read", gen_config.infile_name, "-I", share_path])


## \param[in] cleanup_herwig_scratch Remove `Herwig-scratch` folder after event generation to save disk space
def do_run(gen_config, cleanup_herwig_scratch=True):

    ## this is necessary to make Herwig aware of the name of the run file
    gen_config.genSeq.Herwig7.RunFile = get_runfile_name(gen_config.run_name)

    ## overwrite athena's seed for the random number generator
    if gen_config.runArgs.randomSeed is None:
        gen_config.genSeq.Herwig7.UseRandomSeedFromGeneratetf = False
    else:
        gen_config.genSeq.Herwig7.UseRandomSeedFromGeneratetf = True
        gen_config.genSeq.Herwig7.RandomSeedFromGeneratetf = (
            gen_config.runArgs.randomSeed
        )
    athMsgLog.info(
        hw7Utils.ansi_format_info(
            "Returning to the job options and starting the event generation afterwards"
        )
    )


## Do the run step and re-use an already existing runfile
def do_run_existing_runfile(gen_config):

    ## this is necessary to make Herwig aware of the name of the run file
    gen_config.genSeq.Herwig7.RunFile = gen_config.runfile_name

    ## overwrite athena's seed for the random number generator
    if gen_config.runArgs.randomSeed is None:
        gen_config.genSeq.Herwig7.UseRandomSeedFromGeneratetf = False
    else:
        gen_config.genSeq.Herwig7.UseRandomSeedFromGeneratetf = True
        gen_config.genSeq.Herwig7.RandomSeedFromGeneratetf = (
            gen_config.runArgs.randomSeed
        )

    ## don't break out here so that the job options can be finished and the C++
    ## part of the interface can take over and generate the events
    athMsgLog.info(
        hw7Utils.ansi_format_info(
            "Returning to the job options and starting the event generation afterwards"
        )
    )


# utility functions -----------------------------------------------------------


def herwig_version():

    versions = get_software_versions()
    return " ".join(versions[0].split()[1:])


def thepeg_version():

    versions = get_software_versions()
    return " ".join(versions[1].split()[1:])


def start_banner():

    herwig_version_number = herwig_version()
    thepeg_version_number = thepeg_version()
    herwig_version_space = " ".join(
        ["" for i in range(14 - len(herwig_version_number))]
    )
    thepeg_version_space = " ".join(
        ["" for i in range(14 - len(thepeg_version_number))]
    )

    banner = ""
    banner += "#####################################\n"
    banner += "##   {}   ##\n".format(
        hw7Utils.ansi_format_ok("---------------------------")
    )
    banner += "##   {}   ##\n".format(
        hw7Utils.ansi_format_ok("Starting HERWIG 7 in ATHENA")
    )
    banner += "##   {}   ##\n".format(
        hw7Utils.ansi_format_ok("---------------------------")
    )
    banner += "##                                 ##\n"
    banner += "##   with software versions:       ##\n"
    banner += "##   - Herwig7:    {}{}   ##\n".format(
        herwig_version_number, herwig_version_space
    )
    banner += "##   - ThePEG:     {}{}   ##\n".format(
        thepeg_version_number, thepeg_version_space
    )
    # banner += "##                                            ##\n"
    # banner += "##   with depencency versions                 ##\n"
    # banner += "##   - GoSam\n"
    # banner += "##     - GoSam-Contrib\n"
    # banner += "##     - GoSam\n"
    # banner += "##   - HJets\n"
    # banner += "##   - MadGraph5_aMC@NLO: \n"
    # banner += "##   - NJet: \n"
    # banner += "##   - OpenLoops: \n"
    # banner += "##   - VBFNLO: \n"
    banner += "##                                 ##\n"
    banner += "#####################################\n"
    return banner


def get_software_versions():

    return subprocess.check_output([herwig7_binary, "--version"]).splitlines()


def get_infile_name(run_name="Herwig"):

    return "{}.in".format(run_name)


def get_setupfile_name(run_name="Herwig"):

    return "{}.setupfile.in".format(run_name)


def get_runfile_name(run_name="Herwig"):

    return "{}.run".format(run_name) if not run_name.endswith(".run") else run_name


def write_infile(gen_config, print_infile=True):

    # lock settings to prevent modification from within the job options after infile was written to disk
    #gen_config.default_commands.lock()
    gen_config.commands.lock()

    infile_name = get_infile_name(gen_config.run_name)
    athMsgLog.info("")
    athMsgLog.info("The seed now is {}".format(gen_config.runArgs.randomSeed))
    athMsgLog.info(
        hw7Utils.ansi_format_info("Writing HERWIG 7.2 infile '{}'".format(infile_name))
    )
    try:
        inpcom(gen_config.runArgs,gen_config.run_name,infile_name)
    except:
        raise RuntimeError("Could not write Herwig/Matchbox inputfile")

    if print_infile:
        athMsgLog.info("")
        with open(infile_name, 'r') as inpset:
            for line in inpset:
                athMsgLog.info("  {}".format(line))
            athMsgLog.info("")


def write_setupfile(run_name, commands, print_setupfile=True):

    setupfile_name = get_setupfile_name(run_name)

    if len(commands) > 0:
        if print_setupfile:
            athMsgLog.info("")
        athMsgLog.info("Writing setupfile '{}'".format(setupfile_name))
        try:
            with open(setupfile_name, "w") as setupfile:
                for command in commands:
                    setupfile.write(command + "\n")
        except:
            raise RuntimeError("Could not write Herwig/Matchbox setupfile")

        if print_setupfile:
            athMsgLog.info("")
            for command in commands:
                athMsgLog.info("  {}".format(command))
            athMsgLog.info("")

    else:
        athMsgLog.info("No setupfile commands given.")


## \brief Copy default repository `HerwigDefaults.rpo` to current working directory
##
def get_default_repository():

    shutil.copy(
        "../common_joboptions/HerwigDefaults.rpo", "HerwigDefaults.rpo"
    )


def log(level="info", message=""):

    if level in ["info", "warn", "error"]:
        logger = getattr(athMsgLog, level)
        for line in message.splitlines():
            logger(line)
    else:
        raise ValueError(
            "Unknown logging level'{}' specified. Possible values are 'info', 'warn' or 'error'".format(
                level
            )
        )
