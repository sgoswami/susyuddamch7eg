## \file H7Config.py
## \brief Python convenience snippets providing re-usable bits of settings for use in the jobOptions
import H72Control as hw7Control
import H72Utils as hw7Utils

## Configuration base class for %Herwig7
class H72Conf(object):

    ## Constructor
    def __init__(self, genSeq, runArgs, run_name="Herwig"):

        self.genSeq = genSeq
        self.runArgs = runArgs

        self.run_name = run_name
        # self.seed     = runArgs.randomSeed
        # self.energy   = runArgs.ecmEnergy

        self.me_pdf_name = "NNPDF30_nlo_as0118"
        self.mpi_pdf_name = "NNPDF30_nlo_as0118"

        self.default_commands = hw7Utils.ConfigurationCommands()
        self.commands = hw7Utils.ConfigurationCommands()

    ## \brief Commands applied to all configuration classes before commands from the JobOptions
    ## \todo  Remove `AngularOrdered` settungs once they are included in %Herwig7 by default

    def tune_commands(
        self, ps_tune_name="H7-PS-MMHT2014LO", ue_tune_name="H7.2-Default"
    ):

        cmds = """

"""

        self.ue_tune_commands(tune_name=ue_tune_name)

    def ps_tune_commands(self, tune_name="H7-PS-MMHT2014LO"):

        cmds = """

"""

        if tune_name == "H7-PS-MMHT2014LO":
            cmds += """

"""
        # elif tune_name == "some-other-name":
        #   cmds += self.load_PS_tune(tune_name)
        else:
            raise Exception("Parton shower tune name '{}' unknown".format(tune_name))

        self.commands += cmds

    def load_PS_tune(self, tune_name):

        return """

"""

    def ue_tune_commands(self, tune_name="H7.2-Default"):

        cmds = """

"""

        if tune_name == "H7.2-Default":
            cmds += """

"""
        else:
            raise Exception("Underlying event tune name '{}' unknown".format(tune_name))

        self.commands += cmds

    def load_ue_tune(self, tune_name):

        return """

"""

    def get_dpdf_path(self):
        import os

        cmt_path = os.environ.get("CMAKE_PREFIX_PATH")
        cmt_dir = os.environ.get("BINARY_TAG")

        cmtPaths = cmt_path.split(":")

        for path in cmtPaths:
            pathNow = path + "/InstallArea/" + cmt_dir + "/share/"
            try:
                fileList = os.listdir(pathNow)
            except:
                fileList = []
            if "HerwigDefaults.rpo" in fileList:
                simSharePath = pathNow

        dpdf_path = (
            os.path.dirname(
                os.path.normpath(
                    os.path.join(
                        simSharePath, os.readlink(simSharePath + "HerwigDefaults.rpo")
                    )
                )
            )
            + "/PDF/diffraction/"
        )

        return dpdf_path

    def run(self):
        ## do read and run step in one go
        hw7Control.run(self)
