import math
import os

import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import *

#########################################################################
# SUSY_SimplifiedModel_GG_rpvHF_LLP_aMCH7EG.py
#########################################################################

beamEnergy = -999
if hasattr(runArgs, "ecmEnergy"):
    beamEnergy = runArgs.ecmEnergy / 2.0
else:
    raise RuntimeError("No center of mass energy found.")

evt_multiplier = 3
# nevents = evt_multiplier * evgenConfig.nEventsPerJob

nevts = (
    runArgs.maxEvents * evt_multiplier
    if runArgs.maxEvents > 0
    else evt_multiplier * evgenConfig.nEventsPerJob
)

#########################################################################
# Set masses based on physics short
phys_short = get_physics_short()

decayflavor = phys_short.split("_")[3]
n1_lifetime = (
    phys_short.split("_")[6].replace("ns", "").replace(".py", "").replace("p", "0.")
)
evgenLog.info("lifetime of 1000022 is set to %s ns" % n1_lifetime)

hbar = 6.582119514e-16
n1_decayWidth = str(float(float(hbar) / float(n1_lifetime)))

print("The decay width is ======================================: ", n1_decayWidth)

m_go = "1000021 " + str(float(phys_short.split("_")[4]))  # m gluino =2200GeV
m_n1 = "1000022 " + str(
    float(phys_short.split("_")[5].split(".")[0])
)  # m neutralino1 =200GeV
print("m_gluino = %s GeV, m_n1 = %s GeV" % (m_go, m_n1))
####################################################################

n1_decayheader_temp = "DECAY   1000022  "
n1_decayheader = n1_decayheader_temp + n1_decayWidth + "#Wneu1"
#########################################################################
process = """
set group_subprocesses Auto
set ignore_six_quark_processes False
#set max_t_for_channel 99
#set loop_optimized_output False
#set default_unset_couplings 99
#set zerowidth_tchannel True
#set loop_color_flows True
#set gauge unitary
#set complex_mass_scheme False
#set max_npoint_for_channel 0
set low_mem_multicore_nlo_generation True
import model RPVMSSM_UFO --modelname

define susysq  = su1 su2 su3 su4 su5 su6 sd1 sd2 sd3 sd4 sd5 sd6
define susysq~ = su1~ su2~ su3~ su4~ su5~ su6~ sd1~ sd2~ sd3~ sd4~ sd5~ sd6~

generate    p p > go go  QED=0 RPV=0 / su1 su2 su3 su4 su5 su6 sd1 sd2 sd3 sd4 sd5 sd6 su1~ su2~ su3~ su4~ su5~ su6~ sd1~ sd2~ sd3~ sd4~ sd5~ sd6~ @1
add process p p > go go j  QED=0 RPV=0 / su1 su2 su3 su4 su5 su6 sd1 sd2 sd3 sd4 sd5 sd6 su1~ su2~ su3~ su4~ su5~ su6~ sd1~ sd2~ sd3~ sd4~ sd5~ sd6~ @2
add process p p > go go j j  QED=0 RPV=0 / su1 su2 su3 su4 su5 su6 sd1 sd2 sd3 sd4 sd5 sd6 su1~ su2~ su3~ su4~ su5~ su6~ sd1~ sd2~ sd3~ sd4~ sd5~ sd6~ @3

output

"""

process_dir = new_process(process)
#########################################################################
run_settings = {
    'iseed': '%s' % runArgs.randomSeed,
    'xqcut' : '0',
    'ickkw':'0',
    'time_of_flight': '3',
    'lhe_version': '3.0',
    'small_width_treatment': '1e-15',
    'ktdurham':'500',
    'pdgs_for_merging_cut': '1, 2, 3, 4, 21',
    'cut_decays':'False',
    'nevents': '%s' % nevts
}
modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=run_settings)
#########################################################################
modify_config_card(process_dir=process_dir, settings={"run_mode": "2", "nb_core": "12"})
#########################################################################

madspin_card = "PROC_RPVMSSM_UFO_0/Cards/madspin_card.dat"
if os.access(madspin_card, os.R_OK):
    os.unlink(madspin_card)
else:
    print("ERROR: MADSPIN Card not found")
mscard = open(madspin_card, "w")
mscard.write(
    """
#import PROC_RPVMSSM_UFO_0/Events/run_01/unweighted_events.lhe.gz
#set BW_cut 15
set max_weight_ps_point 400

set seed %s
set spinmode none

define t9 = t s b t~ s~ b~
decay go > t t~ n1 /  su4 su5 su6   sd4 sd5 sd6   su4~ su5~ su6~  sd4~ sd5~ sd6~
decay n1 > t9 t9 t9  / g  su4 su5 su6   sd4 sd5 sd6   su4~ su5~ su6~  sd4~ sd5~ sd6~
launch
"""
    % runArgs.randomSeed
)

mscard.close()

#########################################################################

print(os.getcwd())
if not os.access("PROC_RPVMSSM_UFO_0/Cards/param_card.dat", os.R_OK):
    print("ERROR: Param Card not found")
else:
    os.rename(
        "PROC_RPVMSSM_UFO_0/Cards/param_card.dat",
        "PROC_RPVMSSM_UFO_0/Cards/param_card_old.dat",
    )
    with open("PROC_RPVMSSM_UFO_0/Cards/param_card_old.dat", "r") as oldcard:
        with open("PROC_RPVMSSM_UFO_0/Cards/param_card.dat", "w") as newcard:
            for line in oldcard:
                if "# Mgo" in line:
                    newcard.write(
                        """%s #Mgo

					"""
                        % m_go
                    )
                elif "# Mneu1" in line:
                    newcard.write(
                        """%s #Mneu1

					"""
                        % m_n1
                    )
                # 7.40992706e-02 default SUSY width
                elif "DECAY 1000021" in line:
                    newcard.write(
                        """DECAY  1000021  7.40692706e-02  # Wgo
				# BR          NDA       ID1       ID2       ID3
				1.0000          3      1000022      6         -6
				"""
                    )
                elif "DECAY 1000022" in line:
                    newcard.write(
                        """ %s
				#BR          NDA       ID1       ID2       ID3
				0.5000            3       6         3         5
				0.5000            3      -6        -3        -5
				"""
                        % n1_decayheader
                    )
                else:
                    newcard.write(line)
        newcard.close()
    oldcard.close()

os.remove("PROC_RPVMSSM_UFO_0/Cards/param_card_old.dat")
#########################################################################
# print (os.getcwd())

if not os.access("PROC_RPVMSSM_UFO_0/Cards/me5_configuration.txt", os.R_OK):
    print("ERROR: ME5 Config Card not found")
else:
    os.rename(
        "PROC_RPVMSSM_UFO_0/Cards/me5_configuration.txt",
        "PROC_RPVMSSM_UFO_0/Cards/me5_configuration_old.txt",
    )
    with open("PROC_RPVMSSM_UFO_0/Cards/me5_configuration_old.txt", "r") as oldcard:
        with open("PROC_RPVMSSM_UFO_0/Cards/me5_configuration.txt", "w") as newcard:
            for line in oldcard:
                if "run_mode" in line:
                    newcard.write(
                        """run_mode=2
                                  """
                    )
                elif "nb_core" in line:
                    newcard.write(
                        """nb_core=12
					"""
                    )
                else:
                    newcard.write(line)
        newcard.close()
    oldcard.close()

os.remove("PROC_RPVMSSM_UFO_0/Cards/me5_configuration_old.txt")

#########################################################################

generate(process_dir=process_dir, runArgs=runArgs)
arrange_output(
    process_dir=process_dir, runArgs=runArgs, lhe_version=3, saveProcDir=True
)

######################################################################
evgenConfig.description = (
    "gluino pair production and decay to top quarks and long-lived neutralino, which then decays via RPV lambda323, m_gluino = %s GeV, m_N1 = %s GeV"
    % (m_go, m_n1)
)
evgenConfig.generators = ["aMcAtNlo", "Herwig7"]
evgenConfig.tune = "H7.2-Default"
evgenConfig.contact = ["Soumyananda Goswami <sgoswami@cern.ch>"]
evgenConfig.keywords += [
    "SUSY",
    "RPV",
    "gluino",
    "simplifiedModel",
    "neutralino",
    "longLived",
]
##########################################################################
include("EvgenProdTools/merge_lhe_files.py")
from Herwig7_i.Herwig7_iConf import Herwig7
import sys

sys.path.insert(1, "../common_joboptions/")
import H72Conf

genSeq += Herwig7()

print("Now performing parton showering ...")
generator = H72Conf.H72Conf(genSeq, runArgs, run_name="Herwig")

## configure generator
generator.tune_commands()

#Event Gen Sequence
assert hasattr(genSeq, "Herwig7")
include("EvtGen_i/EvtGen_Fragment.py")
evgenConfig.auxfiles += ['Herwig71Inclusive.pdt']
genSeq.EvtInclusiveDecay.pdtFile = "Herwig71Inclusive.pdt"

## run generator
generator.run()
