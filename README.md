## For MCPROD Request, follow procedures here:

https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SUSYMcRequestProcedure

## To run:
```
mkdir run 
cd run
asetup AthGeneration,21.6.92,here
Gen_tf.py --ecmEnergy=13000 --firstEvent=1 --maxEvents=1000 --randomSeed=1234 --jobConfig=../dsid_[] --outputEVNTFile=[].EVNT.pool.root
```
## To have a log parser check after running:

```
python3 logParser.py -i log.generate -N 10000 -s
```

## Make TRUTH0:
```
asetup AthDerivation,21.2.92.0,here
Reco_tf.py --inputEVNTFile evnt.pool.root --outputDAODFile mc9922.TRUTH0.aMCH7EG.pool.root --reductionConf TRUTH0
```
## Make TRUTH3:
```
asetup AthDerivation,21.2.92.0,here
Reco_tf.py --inputEVNTFile evnt.pool.root --outputDAODFile mc9922.TRUTH3.aMCH7EG.pool.root --reductionConf TRUTH3
```

## Then make ntuples:
```
asetup AnalysisBase,21.2.183,here

# Change input file in tupleExtractor.C
root -l -b -q tupleExtractor.C

#Plot histograms as images
root -l -b -q printHist.C
```
## Extract more info with AnalysisTop

Reference: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisTop

```
asetup AnalysisBase,21.2.183,here
top-xaod my-cuts-ljets.txt filelist.txt
```

## Use this ipynb notebook as an example to get more plots:

https://github.com/jirahandler/AnaTopOutputPlots/blob/main/LQD_VALIDATION.ipynb
